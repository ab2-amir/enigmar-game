using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class nextMessage : MonoBehaviour
{
    private int counter = 1;
    public GameObject m1;
    public GameObject m2;
    public GameObject m3;
    public GameObject m4;
    public GameObject m5;
    public GameObject m6;
    public GameObject m7;
    public void displayNextMessage()
    {
        counter++;
        switch (counter)
        {
            case 2: m2.SetActive(true); break;
            case 3: m3.SetActive(true); break;
            case 4: m4.SetActive(true); break;
            case 5: m5.SetActive(true); break;
            case 6: m1.SetActive(false); m2.SetActive(false); m3.SetActive(false); m4.SetActive(false); m5.SetActive(false); m6.SetActive(true); break;
            case 7: m7.SetActive(true); break;
            case 8: SceneManager.LoadScene("step1"); break;
        }
    }

    public AudioSource clickSound;
    private string targetSceneName = "Welcome"; // Name of the scene to load

    public void ChangeSceneWithSound()
    {
        // Play the click sound
        // GetComponent<AudioSource>().Play();
        clickSound.Play();

        // Delay the scene change by a short duration to allow the sound to play
        Invoke("LoadTargetScene", 0.2f);
    }

    private void LoadTargetScene()
    {
        SceneManager.LoadScene(targetSceneName);
    }
}

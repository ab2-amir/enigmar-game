using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class step1Manager : MonoBehaviour
{
    public InputField fullName;
    public InputField age;
    public Dropdown country;
    public Dropdown armes;
    public Dropdown genre;
    public GameObject messageGoodJob;
    public GameObject megssageBadJob;
    public Button nextBtn;
    public GameObject panelStep1;
    public GameObject panelStep2;
    private string correctFullName = "Eleanor Mitchell";
    private string correctAge = "53";
    private string correctCountry = "USA";
    private string correctGenre = "Man";
    private string correctArme = "Knife";
    private bool isOpen = false;

    private void Start() 
    {
        transform.localScale = Vector2.zero;

        List<string> c = new List<string>
        {
            "Algeria",
            "USA",
            "India"
        };

        country.ClearOptions(); // Clear existing options
        country.AddOptions(c); // Add new options

        List<string> a = new List<string>
        {
            "Gun",
            "Knife",
            "Poison"
        };

        armes.ClearOptions(); // Clear existing options
        armes.AddOptions(a); // Add new options


        List<string> g = new List<string>
        {
            "Man",
            "Woman",
        };

        genre.ClearOptions(); // Clear existing options
        genre.AddOptions(g); // Add new options
    }

    public void submitClues()
    {
        int selectedIndex1 = country.value;
        string selectedCountry = country.options[selectedIndex1].text;

        int selectedIndex2 = armes.value;
        string selectedArme = armes.options[selectedIndex2].text;

        int selectedIndex3 = genre.value;
        string selectedGenre = genre.options[selectedIndex3].text;
        if ((correctFullName == fullName.text) && (correctAge == age.text) && (correctCountry == selectedCountry) && (correctGenre == selectedGenre) && (correctArme == selectedArme))
        {
            megssageBadJob.SetActive(false);
            messageGoodJob.SetActive(true);
            nextBtn.gameObject.SetActive(true);
        }else
        {
            messageGoodJob.SetActive(false);
            megssageBadJob.SetActive(true);
            nextBtn.gameObject.SetActive(false);
        }
    }

    public void OpenClosePhone()
    {
        if(!isOpen)
        {            
            transform.LeanScale(Vector2.one, 0.8f);
            isOpen = true;
        }
        else
        {
            transform.LeanScale(Vector2.zero, 1f);
            isOpen = false;
        }

    }

    public void nextButton()
    {
        panelStep1.SetActive(false);
        panelStep2.SetActive(true);
    }
}

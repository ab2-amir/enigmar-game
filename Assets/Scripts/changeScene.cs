using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class changeScene : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public AudioSource clickSound;
    private string targetSceneName = "Welcome"; // Name of the scene to load

    public void ChangeSceneWithSound()
    {
        // Play the click sound
        // GetComponent<AudioSource>().Play();
        clickSound.Play();

        // Delay the scene change by a short duration to allow the sound to play
        Invoke("LoadTargetScene", 0.2f);
    }

    private void LoadTargetScene()
    {
        SceneManager.LoadScene(targetSceneName);
    }

    private void exit()
    {
        
    }
}

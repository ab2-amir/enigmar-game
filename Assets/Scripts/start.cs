using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Threading;

public class start : MonoBehaviour
{
    public AudioSource clickSound;
    private string targetSceneName = "Introduction"; // Name of the scene to load

    public void ChangeSceneWithSound()
    {
        // Play the click sound
        // GetComponent<AudioSource>().Play();
        clickSound.Play();

        // Delay the scene change by a short duration to allow the sound to play
        Invoke("LoadTargetScene", 0.2f);
    }

    private void LoadTargetScene()
    {
        SceneManager.LoadScene(targetSceneName);
    }

}

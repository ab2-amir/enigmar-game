using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class step2Manager : MonoBehaviour
{
    public InputField phoneNumber;
    public Dropdown objectFound;
    public GameObject messageGoodJob;
    public GameObject megssageBadJob;
    public GameObject phone;
    public GameObject panel;
    public GameObject endScene;
    public Button nextBtn;
    private string correctPhoneNumber = "+122081999";
    private string correctObject = "MUG";
    private bool isOpen = false;

    private void Start() 
    {
        //the value is one because i need the phone to be open when i change from step1 to step2
        transform.localScale = Vector2.one;

        List<string> items = new List<string>
        {
            "CAP",
            "Watch",
            "MUG"
        };

        objectFound.ClearOptions(); // Clear existing options
        objectFound.AddOptions(items); // Add new options
    }

    public void submitClues()
    {
        int selectedIndex = objectFound.value;
        string selectedObject = objectFound.options[selectedIndex].text;
        if ((correctPhoneNumber == phoneNumber.text) && (correctObject == selectedObject))
        {
            panel.SetActive(false);
            phone.SetActive(false);
            endScene.SetActive(true);
        }else
        {
            messageGoodJob.SetActive(false);
            megssageBadJob.SetActive(true);
            nextBtn.gameObject.SetActive(false);
        }
    }

    public void OpenClosePhone()
    {
        if(!isOpen)
        {            
            transform.LeanScale(Vector2.one, 0.8f);
            isOpen = true;
        }
        else
        {
            transform.LeanScale(Vector2.zero, 1f);
            isOpen = false;
        }

    }
}
